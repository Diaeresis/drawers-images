
drawers_images = {}
drawers_images.BLANK_IMAGE = "blank.png"
drawers_images.UNKNOWN_NODE = "unknown_node.png"
drawers_images.modname = core.get_current_modname()
drawers_images.modpath = minetest.get_modpath(drawers_images.modname)
drawers_images.images_path = drawers_images.modpath .. "/textures"
drawers_images.use_cache = minetest.settings:get_bool(drawers_images.modname .. "_use_cache", true)

if drawers_images.use_cache then
    drawers_images.cache = {}
end
drawers_images.addons = {}

local addons_path = drawers_images.modpath .. "/addons/"
local addons = {
    "override_images",
    "mymillwork",
    "moreblocks_saw",
    "stairs",
    "technic_cnc",
    "underch",
    "aotearoa",
}
dofile(drawers_images.modpath .. "/search_filter.lua")
dofile(drawers_images.modpath .. "/utils.lua")
dofile(drawers_images.modpath .. "/build_images.lua")

local function load_addons()
    for _, addon in ipairs(addons) do
        local addon_path = addons_path .. addon .. ".lua"
        if drawers_images.file_exists(addon_path) then
            dofile(addon_path)
        end
    end
end
load_addons()
local function get_from_cache(name)
    if drawers_images.use_cache then
        return drawers_images.cache[name]
    end
    return nil
end

local function put_to_cache(name, image)
    if drawers_images.use_cache then
        drawers_images.cache[name] = image
    end
    return image
end
local function get_from_addons(node, name)
    local image
    for _, addon_function in ipairs(drawers_images.addons) do
        image = addon_function(node, name)
        if image then return put_to_cache(name, image) end
    end
    return nil
end

local function get_node_inventory_image(node)
    if node.inventory_image and #node.inventory_image > 0 then
        return node.inventory_image
    end
    return nil
end

local function get_fallback_image(node, name)
    return put_to_cache(name, drawers_images.get_image_cube(node.tiles)) or drawers_images.BLANK_IMAGE
end

drawers.get_inv_image = function(name)
    if not name or name == "" then return drawers_images.BLANK_IMAGE end
    local node = core.registered_items[name]
    if not node or not node.name then return drawers_images.UNKNOWN_NODE end

    return get_node_inventory_image(node)
        or get_from_cache(name)
        or get_from_addons(node, name)
        or get_fallback_image(node, name)
end


--[[

use 'drawers_images:inventory_image_XXX' pattern when you have updated the mod
XXX = incremental number
]]

core.register_lbm({
    name = "drawers_images:inventory_image_001",
    nodenames = {
        "drawers:wood1", "drawers:wood2", "drawers:wood4",
        "drawers:acacia_wood1", "drawers:acacia_wood2", "drawers:acacia_wood4",
        "drawers:aspen_wood1", "drawers:aspen_wood2", "drawers:aspen_wood4",
        "drawers:pine_wood1", "drawers:pine_wood2", "drawers:pine_wood4",
        "drawers:junglewood1", "drawers:junglewood2", "drawers:junglewood4",
    },
    run_at_every_load = false,
    action = function(pos, node)
        for _, object in ipairs(minetest.get_objects_inside_radius(pos, 0.5)) do
            local luaentity = object:get_luaentity()
            if luaentity and luaentity.updateTexture then
                luaentity:updateTexture ()
            end
        end
    end
})

