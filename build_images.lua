drawers_images.resize = function(image, size)
    -- to correct brightness and contrast of the image need to use correcting overlay drawers_images___correcting_overlay.png
    -- like the Overlay layer mode in GIMP.

    return ("%s^[resize:%sx%s^[overlay:drawers_images___correcting_overlay.png"):format(
        image,
        size,
        size
    )
end
drawers_images.resize_without_overlay = function(image, size)
    return ("%s^[resize:%sx%s"):format(
        image,
        size,
        size
    )
end
drawers_images.AND = function(image, bitmask)
    return ("(%s)^[mask:%s"):format(
        image,
        bitmask
    )
end

drawers_images.OR = function(image, bitmask)
    return ("(%s)^(%s^[opacity:159)"):format(
        image,
        bitmask
    )
end
drawers_images.make_bitmask_file_name = function(filter_name, operator)
    return ("drawers_images_%s_%s_bitmask.png"):format(
        filter_name,
        operator
    )
end

drawers_images.get_image_from_tile = function(tile)
    if type(tile) == "string" then
        return tile
    elseif type(tile) == "table" then
        if type(tile.image) == "string" then
            return tile.image
        elseif type(tile.name) == "string" then
            return tile.name
        end
    end
    return "unknown_node.png"
end

drawers_images.get_image_cube_from_single_tile = function(tiles, tile_num)
    if not tile_num or tile_num < 1 or tile_num > 6 then tile_num = 1 end
    if not tiles or not tiles[tile_num] then return nil end

    local texture = drawers_images.get_image_from_tile(tiles[tile_num])
    return minetest.inventorycube(texture, texture, texture)
end

drawers_images.get_image_cube = function(tiles)
    if not tiles then return nil end
    if #tiles == 6 then
        return minetest.inventorycube(
            drawers_images.get_image_from_tile(tiles[1]),
            drawers_images.get_image_from_tile(tiles[6]),
            drawers_images.get_image_from_tile(tiles[3])
        )
    elseif #tiles == 4 then
        return minetest.inventorycube(
            drawers_images.get_image_from_tile(tiles[1]),
            drawers_images.get_image_from_tile(tiles[4]),
            drawers_images.get_image_from_tile(tiles[3])
        )
    elseif #tiles == 3 then
        return minetest.inventorycube(
            drawers_images.get_image_from_tile(tiles[1]),
            drawers_images.get_image_from_tile(tiles[3]),
            drawers_images.get_image_from_tile(tiles[3])
        )
    elseif #tiles >= 1 then
        return minetest.inventorycube(
            drawers_images.get_image_from_tile(tiles[1]),
            drawers_images.get_image_from_tile(tiles[1]),
            drawers_images.get_image_from_tile(tiles[1])
        )
    end
    return nil
end
