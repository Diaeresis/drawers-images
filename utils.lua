drawers_images.file_exists = function(name)
    if not name then return false end
    local f = io.open(name, "r")
    return f ~= nil and io.close(f)
end

drawers_images.register_addon = function(callback)
    if type(callback) ~= "function" then
        error("drawers_images.register_addon: callback must be a function, got '" .. type(callback) .. "' instead.")
    end
    table.insert(drawers_images.addons, callback)
end
