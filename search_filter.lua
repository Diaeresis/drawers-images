drawers_images.filter = {

    is_match = function(filter_rules, item_to_match)
        local function is_all_match(table, name)
            for _, match_name in ipairs(table) do
                if not name:find(match_name, 1, true) then
                    return false
                end
            end
            return true
        end

        local function is_one_of_match(table, name)
            for _, match_name in ipairs(table) do
                if name:find(match_name, 1, true) then
                    return true
                end
            end
            return false
        end

        local function is_table_match(table, name)
            local one_of_result = true

            if table.one_of then
                one_of_result = is_one_of_match(table.one_of, name)
            end
            local all_of_result = #table == 0 or is_all_match(table, name)

            return all_of_result and one_of_result
        end



        for _, filter_rule in ipairs(filter_rules) do
            if is_table_match(filter_rule, item_to_match) then
                if filter_rule.exclude ~= nil then
                    return not is_table_match(filter_rule.exclude, item_to_match)
                end
                return true
            end
        end
        return false
    end,
}
