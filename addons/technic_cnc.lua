local filters = {
    { name = "technic_cnc_diagonal_truss_cross",    filter = { { "diagonal_truss_cross" } }, },
    { name = "technic_cnc_diagonal_truss",          filter = { { "diagonal_truss" } }, },
    { name = "technic_cnc_arch216",                 filter = { { "arch216" } }, },
    { name = "technic_cnc_onecurvededge_lr",        filter = { { "onecurvededge_lr" } } },
    { name = "technic_cnc_onecurvededge",           filter = { { "onecurvededge" } } },
    { name = "technic_cnc_opposedcurvededge",       filter = { { "opposedcurvededge" } }, },
    { name = "technic_cnc_innercurvededge",         filter = { { "innercurvededge" } } },
    { name = "technic_cnc_twocurvededge_lr",        filter = { { "twocurvededge_lr" } } },
    { name = "technic_cnc_twocurvededge",           filter = { { "twocurvededge" } } },
    { name = "technic_cnc_sphere_quarter",          filter = { { "sphere_quarter" } } },
    { name = "technic_cnc_sphere_half",             filter = { { "sphere_half" } } },
    { name = "technic_cnc_sphere",                  filter = { { "sphere" } } },
    { name = "technic_cnc_oblate_spheroid",         filter = { { "oblate_spheroid" } } },
    { name = "technic_cnc_cylinder_half_corner",    filter = { { "cylinder_half_corner" } } },
    { name = "technic_cnc_d45_slope_216",           filter = { { "d45_slope_216" } } },
    { name = "technic_cnc_element_edge_double",     filter = { { "element_edge_double" } } },
    { name = "technic_cnc_element_edge",            filter = { { "element_edge" } } },
    { name = "technic_cnc_element_t_double",        filter = { { "element_t_double" } } },
    { name = "technic_cnc_element_t",               filter = { { "element_t" } } },
    { name = "technic_cnc_element_cross_double",    filter = { { "element_cross_double" } } },
    { name = "technic_cnc_element_end_double",      filter = { { "element_end_double" } } },
    { name = "technic_cnc_element_straight_double", filter = { { "element_straight_double" } } },
    { name = "technic_cnc_beam216_cross_column",    filter = { { "beam216_cross_column" } } },
    { name = "technic_cnc_beam216_tee",             filter = { { "beam216_tee" } } },
    { name = "technic_cnc_beam216_cross",           filter = { { "beam216_cross" } } },
    { name = "technic_cnc_beam216",                 filter = { { "beam216" } } },
    { name = "technic_cnc_stick",                   filter = { { "stick" } } },
    { name = "technic_cnc_pyramid",                 filter = { { "pyramid" } } },
    { name = "technic_cnc_element_cross",           filter = { { "element_cross" } } },
    { name = "technic_cnc_element_end",             filter = { { "element_end" } } },
    { name = "technic_cnc_element_straight",        filter = { { "element_straight" } } },
}

local build_image = function(node, name)
    if not name:find("_technic_cnc_", 1, true) then return nil end

    for _, filter_rule in ipairs(filters) do
        if drawers_images.filter.is_match(filter_rule.filter, name) then
            local and_bitmask_file_name = drawers_images.make_bitmask_file_name(filter_rule.name, "and")
            local or_bitmask_file_name = drawers_images.make_bitmask_file_name(filter_rule.name, "or")
            local texture = drawers_images.get_image_cube_from_single_tile(node.tiles)
            if texture == nil then return nil end
            texture = drawers_images.resize(texture, 64)
            texture = drawers_images.AND(texture, and_bitmask_file_name)
            texture = drawers_images.OR(texture, or_bitmask_file_name)
            return texture
        end
    end
    return nil
end

drawers_images.register_addon(build_image)
