local filters = {
    {
        name = "stairs",
        filter = {
            { "stairs:stair_", exclude = { one_of = { "_inner", "_outer" } } },
            { "moreblocks:stair_", exclude = { one_of = { "_inner", "_outer", "_half", "_alt", } } },
        }
    },
    {
        name = "slabs",
        filter = {
            { "stairs:slab_" },
            { "moreblocks:slab_", exclude = { one_of = { "_quarter", "_14", "_15" } } },
        }
    },
    {
        name = "stairs_inner",
        filter = {
            { "stairs:stair_", "_inner" },
            { "moreblocks:stair_", "_inner" }
        }
    },
    {
        name = "stairs_outer",
        filter = {
            { "stairs:stair_", "_outer" },
            { "moreblocks:stair_", "_outer" }
        }
    },
}

local build_image = function(node, name)
    if not node.node_box or node.drawtype ~= "nodebox" or not node.tiles or not node.tiles[1] then return nil end
    for _, filter_rule in ipairs(filters) do
        if drawers_images.filter.is_match(filter_rule.filter, name) then
            local and_bitmask_file_name = drawers_images.make_bitmask_file_name(filter_rule.name, "and")
            local or_bitmask_file_name = drawers_images.make_bitmask_file_name(filter_rule.name, "or")
            local texture = drawers_images.get_image_cube_from_single_tile(node.tiles)
            if texture == nil then return nil end
            texture = drawers_images.resize(texture, 64)
            texture = drawers_images.AND(texture, and_bitmask_file_name)
            texture = drawers_images.OR(texture, or_bitmask_file_name)
            return texture
        end
    end
    return nil
end

drawers_images.register_addon(build_image)
