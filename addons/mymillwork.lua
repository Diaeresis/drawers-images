local build_image = function(node, name)
    if node.drawtype ~= "mesh" or not node.mesh then return nil end
    if not string.find(node.name, "mymillwork", 1, true) then return nil end

    local texture = drawers_images.get_image_cube_from_single_tile(node.tiles)
    if texture == nil then return nil end
    local and_bitmask_file_name = "mymillwork___" .. node.mesh:gsub(".obj", ".png")
    texture = drawers_images.resize(texture, 64)
    texture = drawers_images.AND(texture, and_bitmask_file_name)
    return texture
end

drawers_images.register_addon(build_image)
