local filters = {
    {
        name = "underch_platform_high",
        tile_num = 2,
        filter = { { ":platform_high_" } }
    },
    {
        name = "underch_platform_low",
        tile_num = 2,
        filter = { { ":platform_low_" } }
    },
    {
        name = "underch_platform_45_low",
        single_image = true,
        filter = { { ":platform_45_low_" } }
    },
    {
        name = "underch_platform_45",
        single_image = true,
        filter = { { ":platform_45_" } }
    },
    {
        name = "underch__wall",
        filter = { { "_wall" } }
    },
}

local function get_image_from_tile(node, tile_num)
    if not tile_num or tile_num < 1 or tile_num > 6 then tile_num = 1 end
    return drawers_images.get_image_from_tile(node.tiles[tile_num])
end

local build_image = function(node, name)
    if not name:find("underch:", 1, true) then return nil end

    for _, filter_rule in ipairs(filters) do
        if drawers_images.filter.is_match(filter_rule.filter, name) then
            local and_bitmask_file_name = drawers_images.make_bitmask_file_name(filter_rule.name, "and")
            local or_bitmask_file_name = drawers_images.make_bitmask_file_name(filter_rule.name, "or")
            local texture
            if filter_rule.single_image then
                texture = get_image_from_tile(node, filter_rule.tile_num)
            else
                texture = drawers_images.get_image_cube_from_single_tile(node.tiles, filter_rule.tile_num)
            end
            if texture == nil then return nil end
            texture = drawers_images.resize_without_overlay(texture, 64)
            texture = drawers_images.AND(texture, and_bitmask_file_name)
            texture = drawers_images.OR(texture, or_bitmask_file_name)
            return texture
        end
    end
    return nil
end

drawers_images.register_addon(build_image)
