local build_image = function(_, name)
    local png_name = name:gsub(":", "___") .. ".png"
    local file_path = drawers_images.images_path .. "/" .. png_name

    return drawers_images.file_exists(file_path) and png_name or nil
end

drawers_images.register_addon(build_image)
