local filters = {
    {
        name = "moreblocks_micro_12",
        filter = { { ":micro_", "_12", }, }
    },
    {
        name = "moreblocks_micro_14",
        filter = { { ":micro_", "_14", }, }
    },
    {
        name = "moreblocks_micro_15",
        filter = { { ":micro_", "_15", }, }
    },
    {
        name = "moreblocks_micro_1",
        filter = { { ":micro_", "_1", }, }
    },
    {
        name = "moreblocks_micro_2",
        filter = { { ":micro_", "_2", } }
    },
    {
        name = "moreblocks_micro_4",
        filter = { { ":micro_", "_4", }, }
    },
    {
        name = "moreblocks_micro",
        filter = { { ":micro_", }, }
    },
    {
        name = "moreblocks_panel_12",
        filter = { { ":panel_", "_12", }, }
    },
    {
        name = "moreblocks_panel_14",
        filter = { { ":panel_", "_14", }, }
    },
    {
        name = "moreblocks_panel_15",
        filter = { { ":panel_", "_15", }, }
    },
    {
        name = "moreblocks_panel_1",
        filter = { { ":panel_", "_1", }, }
    },
    {
        name = "moreblocks_panel_2",
        filter = { { ":panel_", "_2", }, }
    },
    {
        name = "moreblocks_panel_4",
        filter = { { ":panel_", "_4", }, }
    },
    {
        name = "moreblocks_panel",
        filter = { { ":panel", }, }
    },
    {
        name = "moreblocks_slab_14",
        filter = { { ":slab_", "_14", }, }
    },
    {
        name = "moreblocks_slab_15",
        filter = { { ":slab_", "_15", }, }
    },
    {
        name = "moreblocks_slab_2",
        filter = { { ":slab_", "_2", }, }
    },
    {
        name = "moreblocks_slab_1",
        filter = { { ":slab_", "_1", }, }
    },
    {
        name = "moreblocks_slab_three_sides_u",
        filter = { { ":slab_", "_three_sides_u", }, }
    },
    {
        name = "moreblocks_slab_three_sides",
        filter = { { ":slab_", "_three_sides", }, }
    },
    {
        name = "moreblocks_slab_two_sides",
        filter = { { ":slab_", "_two_sides", }, }
    },
    {
        name = "moreblocks_slab_three_quarter",
        filter = { { ":slab_", "_three_quarter", }, },
    },
    {
        name = "moreblocks_slab_quarter",
        filter = { { ":slab_", "_quarter", }, },
    },
    {
        name = "moreblocks_stair_half",
        filter = { { ":stair_", "_half", }, }
    },
    {
        name = "moreblocks_stair_alt_1",
        filter = { { ":stair_", "_alt_1", }, }
    },
    {
        name = "moreblocks_stair_alt_2",
        filter = { { ":stair_", "_alt_2", }, }
    },
    {
        name = "moreblocks_stair_alt_4",
        filter = { { ":stair_", "_alt_4", }, }
    },
    {
        name = "moreblocks_stair_alt",
        filter = { { ":stair_", "_alt", }, }
    },
    {
        name = "moreblocks_slope_outer_cut_half_raised",
        filter = { { ":slope", "_outer_cut_half_raised", }, }
    },
    {
        name = "moreblocks_slope_outer_cut_half",
        filter = { { ":slope", "_outer_cut_half", }, }
    },
    {
        name = "moreblocks_slope_outer_cut",
        filter = { { ":slope", "_outer_cut" } },
    },
    {
        name = "moreblocks_slope_outer_half_raised",
        filter = { { ":slope", "_outer_half_raised" } },
    },
    {
        name = "moreblocks_slope_outer_half",
        filter = { { ":slope", "_outer_half", }, }
    },
    {
        name = "moreblocks_slope_inner_half_raised",
        filter = { { ":slope", "_inner_half_raised", }, }
    },
    {
        name = "moreblocks_slope_inner_half",
        filter = { { ":slope", "_inner_half", }, }
    },
    {
        name = "moreblocks_slope_inner_cut_half",
        filter = { { ":slope", "_inner_cut_half", }, }
    },
    {
        name = "moreblocks_slope_inner_cut",
        filter = { { ":slope", "_inner_cut", }, }
    },
    {
        name = "moreblocks_slope_half_raised",
        filter = { { ":slope", "_half_raised", }, }
    },
    {
        name = "moreblocks_slope_half",
        filter = { { ":slope", "_half", }, }
    },
    {
        name = "moreblocks_slope_inner",
        filter = { { ":slope", "_inner", }, }
    },
    {
        name = "moreblocks_slope_outer",
        filter = { { ":slope", "_outer", }, }
    },
    {
        name = "moreblocks_slope_cut",
        filter = { { ":slope", "_cut", }, }
    },
    {
        name = "moreblocks_slope",
        filter = { { ":slope", }, }
    },
}

local build_image = function(node, name)
    if not name:find("moreblocks:", 1, true) then return nil end

    for _, filter_rule in ipairs(filters) do
        if drawers_images.filter.is_match(filter_rule.filter, name) then
            local and_bitmask_file_name = drawers_images.make_bitmask_file_name(filter_rule.name, "and")
            local or_bitmask_file_name = drawers_images.make_bitmask_file_name(filter_rule.name, "or")
            local texture = drawers_images.get_image_cube_from_single_tile(node.tiles)
            if texture == nil then return nil end
            texture = drawers_images.resize(texture, 64)
            texture = drawers_images.AND(texture, and_bitmask_file_name)
            texture = drawers_images.OR(texture, or_bitmask_file_name)
            return texture
        end
    end
    return nil
end

drawers_images.register_addon(build_image)
