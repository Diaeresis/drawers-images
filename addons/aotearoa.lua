local filters = {
    {
        name = "aotearoa_gate_closed",
        filter = {
            { ":gate_", "_closed" } },
    },
}

local build_image = function(node, name)
    if not name:find("aotearoa:", 1, true) then return nil end



    for _, filter_rule in ipairs(filters) do
        if drawers_images.filter.is_match(filter_rule.filter, name) then
            local and_bitmask_file_name = drawers_images.make_bitmask_file_name(filter_rule.name, "and")
            local or_bitmask_file_name = drawers_images.make_bitmask_file_name(filter_rule.name, "or")
            local texture = drawers_images.get_image_cube_from_single_tile(node.tiles)
            if texture == nil then return nil end
            texture = drawers_images.resize(texture, 64)
            texture = drawers_images.AND(texture, and_bitmask_file_name)
            texture = drawers_images.OR(texture, or_bitmask_file_name)
            return texture
        end
    end
    return nil
end

drawers_images.register_addon(build_image)
